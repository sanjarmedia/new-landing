import Link from 'next/link'
import { useState } from 'react'
import { NextIcon, PlayIcon } from '../../icons'


export default function SupportDriverApp(){
  const [video, setVideo] = useState(0)
  const isVideo = value => {
    setVideo(video === value ? 0 : value)
  }
  return(
    <>
      {/*Driver's App*/}
      <div className='relative z-10 mt-14 px-5 md:px-12 xl:px-64'>
        <p className='text-primary'>How to’s & Tutorials</p>
        <div className='mt-5 flex items-end md:items-center justify-between'>
          <p className='text-gradient tracking-tight text-5xl font-bold h-16'>Driver’s App</p>
          <button
            className='px-5 h-7 md:h-10 flex items-center bg-background-color rounded-3xl text-primary hover:bg-hover hover:text-white group duration-300'
            onClick={() => isVideo(2)}
          >
            <p className='hidden md:block'>
              See less
            </p>
            {video === 2 ?(
              <NextIcon className='stroke-primary rotate-90 md:ml-3 transition duration-300 group-hover:stroke-white' />
            ) : (
              <NextIcon className='stroke-primary md:ml-3 transition duration-300 -rotate-90 group-hover:stroke-white' />
            )}
          </button>
        </div>
      </div>

      {/*Lessons Video 2*/}
      <div className='text-sm mt-14 px-5 md:px-12 xl:px-64 md:flex items-center justify-between'>
        <div className='md:w-96'>
          <div className='h-80 relative flex items-center justify-center bg-blue-color cursor-pointer'>
            <div
              className='h-20 w-20 flex items-center justify-center rounded-full backdrop-opacity-10 backdrop-invert
            bg-white/30'
            >
              <PlayIcon />
            </div>
          </div>
          <h1 className='font-bold text-3xl mt-10 mb-5'>How to Lesson 1</h1>
          <p>Lorem ipsum dolor sit amet, consec tetur adipiscing tetur adipiscing elit.</p>
        </div>
        <div className='mt-24 md:mt-0 md:w-96'>
          <div className='h-80 relative flex items-center justify-center bg-blue-color cursor-pointer'>
            <div
              className='h-20 w-20 flex items-center justify-center rounded-full backdrop-opacity-10 backdrop-invert
            bg-white/30'
            >
              <PlayIcon />
            </div>
          </div>
          <h1 className='font-bold text-3xl mt-10 mb-5'>How to Lesson 1</h1>
          <p>Lorem ipsum dolor sit amet, consec tetur adipiscing tetur adipiscing elit.</p>
        </div>
        <div className='mt-24 md:mt-0 md:w-96'>
          <div className='h-80 relative flex items-center justify-center bg-blue-color cursor-pointer'>
            <div
              className='h-20 w-20 flex items-center justify-center rounded-full backdrop-opacity-10 backdrop-invert
            bg-white/30'
            >
              <PlayIcon />
            </div>
          </div>
          <h1 className='font-bold text-3xl mt-10 mb-5'>How to Lesson 1</h1>
          <p>Lorem ipsum dolor sit amet, consec tetur adipiscing tetur adipiscing elit.</p>
        </div>
      </div>
      {video === 2 ? (
        <div className='transition duration-500 opacity-100 text-sm mt-14 px-5 md:px-12 xl:px-64 md:flex items-center justify-between'>
          <Link href='/lessons'>
            <div className='md:w-96'>
              <div className='h-80 relative flex items-center justify-center bg-blue-color cursor-pointer'>
                <div
                  className='h-20 w-20 flex items-center justify-center rounded-full backdrop-opacity-10 backdrop-invert
            bg-white/30'
                >
                  <PlayIcon />
                </div>
              </div>
              <h1 className='font-bold text-3xl mt-10 mb-5'>How to Lesson 1</h1>
              <p>Lorem ipsum dolor sit amet, consec tetur adipiscing tetur adipiscing elit.</p>
            </div>
          </Link>
          <Link href='/lessons'>
            <div className='md:w-96'>
              <div className='h-80 relative flex items-center justify-center bg-blue-color cursor-pointer'>
                <div
                  className='h-20 w-20 flex items-center justify-center rounded-full backdrop-opacity-10 backdrop-invert
            bg-white/30'
                >
                  <PlayIcon />
                </div>
              </div>
              <h1 className='font-bold text-3xl mt-10 mb-5'>How to Lesson 1</h1>
              <p>Lorem ipsum dolor sit amet, consec tetur adipiscing tetur adipiscing elit.</p>
            </div>
          </Link>
          <Link href='/lessons'>
            <div className='md:w-96'>
              <div className='h-80 relative flex items-center justify-center bg-blue-color cursor-pointer'>
                <div
                  className='h-20 w-20 flex items-center justify-center rounded-full backdrop-opacity-10 backdrop-invert
            bg-white/30'
                >
                  <PlayIcon />
                </div>
              </div>
              <h1 className='font-bold text-3xl mt-10 mb-5'>How to Lesson 1</h1>
              <p>Lorem ipsum dolor sit amet, consec tetur adipiscing tetur adipiscing elit.</p>
            </div>
          </Link>
        </div>
      ) : (
        <div className='transition duration-500 opacity-0'></div>
      )}

      <hr className='mx-5 md:mx-12 xl:mx-64 mt-28' />

    </>
  )
}